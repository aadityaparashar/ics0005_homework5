
import java.util.*;
import java.lang.System.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }
   
   public static Node parsePostfix (String s) {


      if (s.contains("\t")) {
         throw new RuntimeException("Error: Tab char present in: " +s);
      }
      else if (s.contains("()")) {
         throw new RuntimeException("Error: Empty Subtree present in: " +s);
      }
      else if (s.contains(" ")) {
         throw new RuntimeException("Error: Space present in: " +s);
      }
      else if (( !(s.contains("(")) && s.contains(")")) || (s.contains("(") && !(s.contains(")")))) {
         throw new RuntimeException("Error: Error in sub-tree declaration Input: " +s);
      }
      else if (s.contains("((") && s.contains("))")) {
         throw new RuntimeException("Error: Double brackets present in: " +s);
      }
      else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) {
         throw new RuntimeException("Error: Two roots present in: " +s);
      }
      else if (s.contains(",,")) {
         throw new RuntimeException("Error: Double commas present in: " +s);
      }

      Stack<Node> stack = new Stack();
      Node node = new Node(null, null, null);

      boolean replacingRoot = false;

      for (int i = 0; i < s.length(); i++) {

         String token = s.substring(i,i+1).trim();

         if (s.substring(i,i+1).equals("(")) {

            if (replacingRoot) {
               throw new RuntimeException("Error: Trying to replace root. Input: " +s);
            }

            stack.push(node);

            node.firstChild = new Node(null, null, null);

            node = node.firstChild;

            if (s.substring(i+1,i+2).trim().equals(",")) {
               throw new RuntimeException("Error: Comma after node. Input: " +s);
            }
         }

         else if (s.substring(i,i+1).equals(")")) {
            node = stack.pop();

            if (stack.size() == 0) {
               replacingRoot = true;
            }
         }

         else if (s.substring(i,i+1).equals(",")) {

            if (replacingRoot) {
               throw new RuntimeException("Error: Trying to replace root. Input: " + s);
            }
            node.nextSibling = new Node(null, null, null);
            node = node.nextSibling;
         }

         else {
            if (node.name == null) {
               node.name = token;
            }
            else {
               node.name += token;
            }
         }
      }

      return node;

   }

   public String leftParentheticRepresentation() {

      //String toReturn="";
      StringBuffer sb = new StringBuffer("");

      //toReturn = toReturn + this.name;
      sb.append(this.name);

      if (this.firstChild != null) {
         //toReturn = toReturn + "(";
         //toReturn = toReturn + this.firstChild.leftParentheticRepresentation();
         //toReturn = toReturn + ")";

         sb.append("(");
         sb.append(this.firstChild.leftParentheticRepresentation());
         sb.append(")");
      }

      if (this.nextSibling != null) {
         //toReturn = toReturn + ",";
         //toReturn = toReturn + this.nextSibling.leftParentheticRepresentation();

         sb.append(",");
         sb.append(this.nextSibling.leftParentheticRepresentation());
      }

      //return toReturn;
      return sb.toString();
   }

   public static void main (String[] param) {
      String s = "(B1,C)A";
      //String s = "(B1,C(C1(C11,C12(C121,C122,C123)),C2))A"; // Output: A(B1,C(C1(C11,C12(C121,C122,C123)),C2))
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}

